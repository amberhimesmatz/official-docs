The files in this project can be used to build an Official Docs book, containing short guides to various aspects of Drupal.

The source uses AsciiDoc markdown format, which can be compiled into DocBook format, which in turn can be compiled into HTML, PDF, and various e-book formats. The [AsciiDoc Display module](https://www.drupal.org/project/asciidoc_display) can be used to display special HTML output in a Drupal site.

See also the User Guide project, which provides a tutorial-style guide to Drupal for beginners: https://www.drupal.org/project/user_guide

COPYRIGHT AND LICENSE
---------------------

See the ASSETS.yml file in this directory, and files it references, for copyright and license information for the text source files and images in this project. Output files produced and displayed by this project also must contain copyright/license information.

Code in this project, consisting of scripts for generating output from the source files, and code for generating automatic screen captures, is licensed under the GNU/GPL version 2 and later license.


FILE ORGANIZATION
-----------------

This project contains the following directories:

* source

The AsciiDoc source for the guides is in language subdirectories of the source directory. The index file is called "guides.txt", and it has include statements for the other files that make up the guides.

* guidelines / templates

Guidelines and templates for contributors to this project are in the guidelines and templates directories, respectively. The guidelines are in the form of another AsciiDoc manual, with guidelines.txt as the index file. There are separate templates for topics covering tasks and concepts.

* scripts / output

To build both the Guides and Guidelines books, use the scripts in the scripts directory; see below for more information. Currently the Guidelines document scripts only produce HTML output for the AsciiDoc Display module, and the Guides scripts produce HTML output as well as PDF and other e-books.

The output the scripts produces lands in the output directory. Subdirectories html and html_feed of that is the output for the AsciiDoc Display module; e-books land in the ebooks subdirectory.


ASCIIDOC OUTPUT BUILD SCRIPTS
-----------------------------

The Guides and Guidelines are both set up with scripts to make output compatible with the [AsciiDoc Display module](https://www.drupal.org/project/asciidoc_display), as well as PDF and other e-book output (the scripts are adapted from the sample scripts in that project, and only the Guides book is currently set up to make e-book output).

To run the scripts, you will need several open-source tools:

- AsciiDoc (for any output): http://asciidoc.org/INSTALL.html
- DocBook (for any output): http://docbook.org or http://www.dpawson.co.uk/docbook/tools.html
- FOP (for PDF): http://xmlgraphics.apache.org/fop/
- Calibre (for MOBI): http://calibre-ebook.com/

On a Linux machine, you can use one of these commands to install all the tools:

    apt-get install asciidoc docbook fop calibre
    yum install asciidoc docbook fop calibre

On a Mac:

    brew install asciidoc xmlto

Note that these scripts do not work with all available versions of AsciiDoc and Docbook tools. They have been tested to work with:

- asciidoc - version 8.6.9
- xmlto - version 0.0.25

You can check versions by typing:

    asciidoc --version
    xmlto --version

on the command line.

### Compiling AsciiDoc to HTML

You can compile the AsciiDoc documentation into HTML files by running the following from the repository's root directory:

    composer mac-compile-docs
    composer linux-compile-docs

depending on your operating system.

### IDE Configuration

If you would like to make a substantial contribution, take a few minutes to set up your local environment for efficient, automated document compilation.

If you use PHPStorm, you can configure it to render render a preview of AsciiDoc files by installing the [AsciiDoc plugin](https://plugins.jetbrains.com/plugin/7391-asciidoc).

### Managing GitLab CI

This section is for repository maintainers only.

The CI scripts for merge requests live it `.gitlab-ci.yml`. These use the [grasmash/asciidoc docker image](https://hub.docker.com/r/grasmash/asciidoc/) as a base and will compile and validate the documentation.

To update the base image:

1. Modify Dockerfile
2. `docker build -t asciidoc .`
3. `docker tag asciidoc grasmash/asciidoc`
4. `docker push grasmash/asciidoc`

To locally execute the GitLab CI scripts:

1. Install [GitLab Runner](https://docs.gitlab.com/runner/install/)
2. `gitlab-runner exec docker compile_docs`