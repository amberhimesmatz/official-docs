[[attributions]]
=== Guide-Wide Attributions

[role="summary"]
List of contributors involved in project management and guide-wide writing/editing tasks.

(((Content attributions for this document)))
(((Attributions for this document)))
(((Copyright for this document)))

This guide was written by contributors to the Drupal open-source
project, and is licensed under the
https://creativecommons.org/licenses/by-sa/2.0/[CC BY-SA 2.0] license. See
<<copyright>> for more information. Details about the contributors for
guide-wide tasks are below. For individual topics, the attributions for writing,
editing, and/or translating are at the end of each topic.


==== Project coordination of original (English, Drupal 8) text

Writing of the initial version of this guide was coordinated by the following
people:

* https://www.drupal.org/u/grasmash[Matthew Grasmick] - Content oversight

* https://www.drupal.org/u/jhodgdon[Jennifer Hodgdon] - Technical/scripts help


==== Writing, editing, and testing of original text

The following people contributed to guide-wide writing:

Guide-wide editing was done by the following people:


==== Images

The following images were used in this guide:
